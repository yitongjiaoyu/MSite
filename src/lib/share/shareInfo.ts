
import store from '../../store/store'
import { stringifyQuery, parseQuery } from '../utils'
const origin = location.origin || (location.protocol + '//' + location.host)
export function getShareInfo ({
  routeName,
  subject = '',
  userName = store.state.userInfo.RealName || store.state.userInfo.Phone,
  sharePath,
}: GetShareInfoParams): ShareInfoObj {
  const title = `${userName}${store.state.instInfo.TrainingInstitutionId === 268 ? '邀请您报名2019年成人高考'
    : '邀请您加入学习'}，${store.state.instInfo.TrainingName}为您保驾护航`
  const descLink = getShareDescLink(routeName, subject)
  const computedSharePath = sharePath || descLink.link || (location.pathname + location.search)
  const [sharedPath, oldQuery] = computedSharePath.split('?')

  // eslint-disable-next-line no-console
  // console.table(parseQuery(`${match[2]}&userId=${store.state.userInfo.PersonId || ''}`))
  const userIdQuery = `userId=${store.state.userInfo.PersonId || ''}`
  const newQuery = oldQuery ? stringifyQuery(parseQuery(`${oldQuery}&${userIdQuery}`)) : userIdQuery
  const shareUrl = decodeURIComponent(`${origin}${sharedPath}${newQuery ? '?' + newQuery : ''}`)
  // eslint-disable-next-line no-console
  // console.table({
  //   path: sharedPath,
  //   oldQuery,
  //   userIdQuery,
  //   newQuery,
  //   shareUrl,
  //   sharePath,
  // })
  return { title, desc: descLink.desc, link: shareUrl }
}

function getShareDescLink (routeName: string, subject: string) {
  const industryName = store.state.instInfo.Industry
  const desc = store.state.instInfo.TrainingInstitutionId === 268 ? '2019年成人高考考前辅导利器助您顺利录取'
    : `${industryName ? industryName.join('，') + '。' : ''}线上免费试听，考试自测。论学习我们是认真的。`
  switch (routeName) {
    case 'classDetail':
    case 'freeDetail':
      return { desc }
    case 'examTest':
    case 'examResult':
      const testQuery = parseQuery(location.search)
      return { desc,
        link: `/examTest?Type=${testQuery.Type}&PaperId=${testQuery.PaperId}&CourseId=${testQuery.CourseId}\
&QuestionType=${testQuery.QuestionType || 0}&ShowType=${testQuery.ShowType || 0}` }
    case 'receiveSS':
    case 'scholarShipList':
    case 'studentScholarShip':
      return {
        desc,
        link: '/shareOutPage',
      }
    case 'home':
    default:
      return { desc, link: '/' }
  }
}

interface ShareInfoObj {
  title: string
  desc: string
  link: string
}
interface GetShareInfoParams {
  routeName: string
  subject: string
  userName?: string
  sharePath?: string
}
// interface IShareInfo {
//   [pageName: string]: ShareInfoObj
// }

export const APIS_CONFIG: IAPIS = {
  'DailyPractice/DetailWithAnalysis': {
    showLoading: true,
  },
  'VisitorExam/DetailWithAnalysis': {
    showLoading: true,
  },
  'VisitorExam/Detail': {
    showLoading: true,
  },
  'Exam/DetailWithAnalysis': {
    showLoading: true,
  },
  'DailyPractice/Detail': {
    showLoading: true,
  },
  'Exam/Detail': {
    showLoading: true,
  },
  'UserCollectQuestion/PaperDetail': {
    showLoading: true,
  },
  'UserWrongQuestion/PaperDetail': {
    showLoading: true,
  },
  'ChapterPractice/DetailWithAnalysis': {
    showLoading: true,
  },
}

interface IAPIS {
  [apiName: string]: APIConfig
}
interface APIConfig {
  url?: string
  showLoading: boolean
  shouldLogin?: boolean
}

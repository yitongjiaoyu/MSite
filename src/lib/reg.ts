export const PWD_REG_STR = '^[\\w_]{6,18}$'
export const MOBILE_STR = '^1\\d{10}$'
export const CERT_STR = `(^[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9X]$)\
|(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{2}[0-9X]$)`
export const NAME_STR = /^([\u4e00-\u9fa5]{1,10})$/
export const PWD_TIP = '请输入6~18位的数字和大小写字母！'

let Hammer: any
if (typeof window !== 'undefined') {
  // eslint-disable-next-line
  Hammer = require('hammerjs');
  (window as any).Hammer = Hammer
  delete Hammer.defaults.cssProps.userSelect
}

const capitalize = (str: string) => str.charAt(0).toUpperCase() + str.slice(1).toLowerCase()

const Clobber = {
  bind (el: any, binding: any) {
    if (!el.$hammer) {
      el.$hammer = new Hammer.Manager(el)
    }

    const gesture = capitalize(binding.arg)
    const direction = Object.keys(binding.modifiers).length
      ? Object.keys(binding.modifiers)[0].toUpperCase() : 'ALL'
    const options = {
      direction: Hammer[`DIRECTION_${direction}`],
    }
    // let options
    // if (gesture === 'swipe') {
    //   const direction = binding.modifiers.direction || 'ALL'
    //   options = {
    //     direction: Hammer[`DIRECTION_${direction.toUpperCase()}`],
    //   }
    // }

    try {
      el.$hammer.add(new Hammer[gesture](options))
    } catch (error) {
      console.log(error)
      throw new Error('[vue-clobber] invalid gesture')
    }

    if (typeof binding.value !== 'function') {
      throw new Error('[vue-clobber] invalid handler')
    }

    el.$hammer.on(gesture.toLowerCase(), binding.value)
  },
  unbind (el: any) {
    el.$hammer.destroy()
    el.$hammer = null
  },
}

function plugin (Vue: any) {
  Vue.directive('clobber', Clobber)
}

export default plugin
// export {
//   Clobber,
// }

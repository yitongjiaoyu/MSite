import Vue from 'vue'
import Router, { RouteConfig } from 'vue-router'
import Home from './views/Home.vue'
import { AppInfo } from 'lib/appMethods'

Vue.use(Router)

// setSharePage: 1当前页面分享，2当前页面有分享按钮特殊处理
export const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "home" */'@/views/Home.vue'),
    meta: {
      titleIsInstName: true,
      tabBar: true,
      setSharePage: 1,
    },
  },
  {
    path: '/map',
    name: 'map',
    component: () => import(/* webpackChunkName: "map" */'@/views/Others/Map.vue'),
    meta: {
      title: '学校地址',
    },
  },
  {
    path: '/examIndex',
    name: 'examIndex',
    component: () => import(/* webpackChunkName: "examHome" */'@/views/Exam/ExamIndex.vue'),
    meta: {
      title: '考试',
      // tabBar: true,
    },
  },
  {
    path: '/examIntro',
    name: 'examIntro',
    component: () => import(/* webpackChunkName: "examIntro" */'@/views/Exam/TestIntro.vue'),
    meta: {
      title: '题型介绍',
    },
  },
  {
    path: '/examTest',
    name: 'examTest',
    component: () => import(/* webpackChunkName: "examTest" */'@/views/Exam/Test.vue'),
    meta: {
      title: '考试',
      keepAlive: true,
      setSharePage: 2,
      requireAuth: true,
    },
  },
  {
    path: '/exercises',
    name: 'exercises',
    component: () => import(/* webpackChunkName: "examTest" */'@/views/Exam/Exercises.vue'),
    meta: {
      keepAlive: true,
      title: '章节练习',
      // appNoHeader: true,
    },
  },
  {
    path: '/dailyExam',
    name: 'dailyExam',
    component: () => import(/* webpackChunkName: "dailyExam" */'@/views/Exam/DailyTest.vue'),
    meta: {
      title: '每日考',
      keepAlive: true,
    },
  },
  {
    path: '/dailyExamComplete',
    name: 'dailyExamComplete',
    component: () => import(/* webpackChunkName: "dailyExamComplete" */
      '@/views/Exam/DailyExam_Complete.vue'),
    meta: {
      title: '每日考',
      keepAlive: true,
    },
  },
  {
    path: '/examCard',
    name: 'examCard',
    component: () => import(/* webpackChunkName: "examCard" */'@/views/Exam/TestCard.vue'),
    meta: {
      title: '答题卡',
    },
  },
  {
    path: '/examResult',
    name: 'examResult',
    component: () => import(/* webpackChunkName: "examResult" */'@/views/Exam/TestResult.vue'),
    meta: {
      title: '答题结果',
      // right: { share: AppInfo.isApp, onClick: 'onShare' },
      titleFrom: true,
      keepAlive: true,
      setSharePage: 2,
    },
  },
  {
    path: '/reciptePoints',
    name: 'reciptePoints',
    component: () => import(/* webpackChunkName: "reciptePoints" */
      '@/views/Class/ReciptePoints.vue'),
    meta: {
      title: '知识点',
      titleFrom: true,
    },
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */'@/views/Login/Login.vue'),
    meta: {
      title: '登录',
    },
  },
  {
    path: '/retrievePassword',
    name: 'retrievePassword',
    component: () => import(/* webpackChunkName: "retrievePassword" */
      '@/views/Login/RetrievePassword.vue'),
    meta: {
      title: '找回密码',
    },
  },
  {
    path: '/register',
    name: 'register',
    component: () => import(/* webpackChunkName: "register" */'@/views/Login/Register.vue'),
    meta: {
      title: '注册',
    },
  },
  {
    path: '/agreement',
    name: 'agreement',
    component: () => import(/* webpackChunkName: "agreement" */'@/views/Login/Agreement.vue'),
    meta: {
      title: '用户协议',
    },
  },
  {
    path: '/materialBank',
    name: 'materialBank',
    component: () => import(/* webpackChunkName: "" */'@/views/MaterialBank/MaterialBank.vue'),
    meta: {
      title: '资料库',
    },
  },
  {
    path: '/news',
    name: 'news',
    component: () => import(/* webpackChunkName: "news" */'@/views/News/News.vue'),
    meta: {
      title: '资讯',
    },
  },
  {
    path: '/newsDetail',
    name: 'newsDetail',
    component: () => import(/* webpackChunkName: "newsDetail" */'@/views/News/NewsDetail.vue'),
    meta: {
      title: '资讯详情',
    },
  },
  {
    path: '/newsList',
    name: 'newsList',
    component: () => import(/* webpackChunkName: "newsList" */'@/views/News/NewsList.vue'),
    meta: {
      title: '考前读',
    },
  },
  {
    path: '/mine',
    name: 'myIndex',
    component: () => import(/* webpackChunkName: "myIndex" */'@/views/Mine/MyIndex.vue'),
    meta: {
      title: '我的',
      tabBar: true,
      requireAuth: true,
    },
  },
  {
    path: '/myInfo',
    name: 'myInfo',
    component: () => import(/* webpackChunkName: "MyInfo" */'@/views/Mine/MyInfo.vue'),
    meta: {
      title: '个人中心',
      requireAuth: true,
    },
  },
  {
    path: '/changeSex',
    name: 'changeSex',
    component: () => import(/* webpackChunkName: "changeSex" */'@/views/Mine/ChangeSex.vue'),
    meta: {
      title: '更改性别',
      requireAuth: true,
    },
  },
  {
    path: '/changePhone',
    name: 'changePhone',
    component: () =>
      import(/* webpackChunkName: "changePhone" */'@/views/Mine/ChangePhone.vue'),
    meta: {
      title: '更改手机号码',
      requireAuth: true,
    },
  },
  {
    path: '/changeCardNo',
    name: 'changeCardNo',
    component: () =>
      import(/* webpackChunkName: "changeCardNo" */'@/views/Mine/ChangeCardNo.vue'),
    meta: {
      title: '更改身份证号',
      requireAuth: true,
    },
  },
  {
    path: '/changeRealName',
    name: 'changeRealName',
    component: () =>
      import(/* webpackChunkName: "changeRealName" */'@/views/Mine/ChangeRealName.vue'),
    meta: {
      title: '更改姓名',
      requireAuth: true,
    },
  },
  {
    path: '/changePwd',
    name: 'changePwd',
    component: () =>
      import(/* webpackChunkName: "changePwd" */'@/views/Mine/ChangePwd.vue'),
    meta: {
      title: '修改密码',
      requireAuth: true,
    },
  },
  {
    path: '/myMoreInfo',
    name: 'myMoreInfo',
    component: () =>
      import(/* webpackChunkName: "myMoreInfo" */'@/views/Mine/MyMoreInfo.vue'),
    meta: {
      title: '报名信息',
      requireAuth: true,
    },
  },
  {
    path: '/chgMoreInfo',
    name: 'chgMoreInfo',
    component: () =>
      import(/* webpackChunkName: "chgMoreInfo" */'@/views/Mine/ChgMoreInfo.vue'),
    meta: {
      titleFrom: true,
      title: '修改用户信息',
      requireAuth: true,
    },
  },
  {
    path: '/pay',
    name: 'pay',
    component: () => import(/* webpackChunkName: "pay" */'@/views/Pay/Pay.vue'),
    meta: {
      title: '去付款',
      // requireAuth: true,
    },
  },
  {
    path: '/paySuccess',
    name: 'paySuccess',
    component: () => import(/* webpackChunkName: "ps" */'@/views/Pay/PaySuccess.vue'),
    meta: {
      title: '支付成功',
    },
  },
  {
    path: '/payFail',
    name: 'payFail',
    component: () => import(/* webpackChunkName: "ps" */'@/views/Pay/PayFail.vue'),
    meta: {
      title: '支付失败',
    },
  },
  {
    path: '/outDate',
    name: 'outDate',
    component: () => import(/* webpackChunkName: "outDate" */'@/views/Error/OutDate.vue'),
    meta: {
      title: '😂',
    },
  },
  {
    path: '/teacherInfo',
    name: 'teacherInfo',
    component: () => import(/* webpackChunkName: "teacherInfo" */'@/views/Index/TeacherInfo.vue'),
    meta: {
      title: '名师介绍',
    },
  },
  {
    path: '/freeCourse',
    name: 'freeCourse',
    component: () => import(/* webpackChunkName: "freeCourse" */'@/views/Class/FreeCourse.vue'),
    meta: {
      title: '免费课程',
    },
  },
  {
    path: '/myCollection',
    name: 'myCollection',
    component: () => import(/* webpackChunkName: "myCollection" */'@/views/Mine/MyCollection.vue'),
    meta: {
      title: '我的收藏',
      requireAuth: true,
    },
  },
  {
    path: '/myWrong',
    name: 'myWrong',
    component: () => import(/* webpackChunkName: "myWrong" */'@/views/Mine/MyWrong.vue'),
    meta: {
      title: '我的错题',
      requireAuth: true,
    },
  },
  {
    path: '/myClass',
    name: 'myClass',
    component: () => import(/* webpackChunkName: "myClass" */'@/views/Mine/MyClass.vue'),
    meta: {
      titleFrom: true,
      title: '我的套餐',
      requireAuth: true,
    },
  },
  {
    path: '/myCourseTable',
    name: 'myCourseTable',
    component: () => import(/* webpackChunkName: "mySchedule" */'@/views/Mine/MyCourseTable.vue'),
    meta: {
      title: '我的课程表',
      requireAuth: true,
    },
  },
  {
    path: '/class',
    name: 'class',
    component: () => import(/* webpackChunkName: "class" */'@/views/Class/Class.vue'),
    meta: {
      title: '班级',
      // tabBar: true,
    },
  },
  {
    path: '/classDetail',
    name: 'classDetail',
    component: () => import(/* webpackChunkName: "classDetail" */'@/views/Class/ClassDetail.vue'),
    meta: {
      titleFrom: true,
      title: '班级',
      setSharePage: 1,
    },
  },
  {
    path: '/classSearch',
    name: 'classSearch',
    component: () => import(/* webpackChunkName: "classSearch" */'@/views/Class/ClassSearch.vue'),
    meta: {
      title: '搜索',
    },
  },
  {
    path: '/freeDetail',
    name: 'freeDetail',
    component: () =>
      import(/* webpackChunkName: "freeDetail" */'@/views/Class/FreeCourseDetail.vue'),
    meta: {
      titleFrom: true,
      title: '免费课程',
      setSharePage: 1,
    },
  },
  {
    path: '/aposter',
    name: 'anotherPoster',
    component: () =>
      import(/* webpackChunkName: "poster" */'@/views/Others/AnotherPoster.vue'),
    meta: {
      titleIsInstName: true,
      setSharePage: 1,
    },
  },
  {
    path: '/liveList',
    name: 'liveList',
    component: () => import(/* webpackChunkName: "liveList" */
      '@/views/Live/LiveList.vue'),
    meta: {
      title: '直播',
      tabBar: true,
    },
  },
  {
    path: '/live',
    name: 'live',
    component: () => import(/* webpackChunkName: "live" */
      '@/views/Live/Live.vue'),
    meta: {
      titleFrom: true,
      title: '直播',
    },
  },
  {
    path: '/questionCorrective',
    name: 'questionCorrective',
    component: () => import(/* webpackChunkName: "questionCorrective" */
      '@/views/Exam/QuestionCorrective.vue'),
    meta: {
      titleFrom: true,
      title: '试题纠错',
    },
  },
  {
    path: '/privacyPolicy',
    name: 'privacyPolicy',
    component: () => import(/* webpackChunkName: "privacyPolicy" */
      '@/views/Login/PrivacyPolicy.vue'),
    meta: {
      titleFrom: true,
      title: '隐私政策',
    },
  },
  {
    path: '/chat',
    name: 'chat',
    component: () => import(/* webpackChunkName: "chat" */
      '@/views/Abc/Chat.vue'),
    meta: {
      title: '聊天',
    },
  },
  {
    path: '/courseIndex',
    name: 'courseIndex',
    component: () => import(/* webpackChunkName: "courseIndex" */'@/views/Course/CourseIndex.vue'),
    meta: {
      title: '套餐',
      tabBar: true,
    },
  },
  {
    path: '/aboutUs',
    name: 'aboutUs',
    component: () => import(/* webpackChunkName: "aboutUs" */'@/views/Others/AboutUs.vue'),
    meta: {
      title: '关于我们',
    },
  },
  {
    path: '/videoRecords',
    name: 'videoRecords',
    component: () => import(/* webpackChunkName: "videoRecords" */'@/views/Index/VideoRecords.vue'),
    meta: {
      title: '视频学习记录',
      requireAuth: true,
    },
  },
  {
    path: '/examRecords',
    name: 'examRecords',
    component: () => import(/* webpackChunkName: "examRecords" */'@/views/Index/ExamRecords.vue'),
    meta: {
      title: '题库做题记录',
      requireAuth: true,
    },
  },
]

export default new Router({
  // mode: 'history',
  // base: '/',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
})

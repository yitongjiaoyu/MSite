import Vue from 'vue'
import { isiOS } from '@/lib/utils'

export default Vue.extend({
  mounted () {
    if (isiOS) {
      console.log('bind ios blur')
      document.querySelectorAll('input,textarea').forEach(el => {
        el.addEventListener('blur', this.onIosBlur, false)
      })
    }
  },
  beforeDestroy () {
    if (isiOS) {
      document.querySelectorAll('input,textarea').forEach(el => {
        el.removeEventListener('blur', this.onIosBlur, false)
      })
    }
  },
  methods: {
    onIosBlur () {
      console.log('9999999933')
      document.body.scrollTop = document.documentElement!.scrollTop = 0
    },
  },
})

import Vue from 'vue'
import { mapState } from 'vuex'
import { AppInfo } from '@/lib/appMethods'
import { syncRequest } from '@/plugins/http.plugin'

export default Vue.extend({
  data () {
    return {
      userId: this.$store.state.userInfo.UserId,
      PaperTestHistoryId: this.$store.state.question.PaperTestHistoryId,
      isSubmiting: false,
    }
  },
  computed: {
    ...mapState(['question']) as MappedState,
  },
  methods: {
    onSubmit (fromCard = true) {
      if (this.isSubmiting) {
        return
      }
      this.isSubmiting = true
      // const { CourseId, PaperId, Type } = this.$route.query
      this.submitResult(true).then(res => {
        const methodName = fromCard ? 'replace' : 'push'
        this.$router[methodName]({ name: 'examResult',
          query: {
            ...this.$route.query,
            title: this.question.PaperName,
            ...res,
          },
        })
      }).finally(() => {
        this.isSubmiting = false
      })
    },
    syncSubmitResult () {
      const { Type, CourseId, PaperId, ClassId } = this.$route.query
      const data = this.getSubmitData()
      syncRequest({
        url: 'ytjysite/TPaper/SubmitPaper',
        method: 'post',
        data: {
          PaperId,
          Items: data.answers,
          LastQuestionId: this.LastQuestionId,
          PaperTestHistoryId: this.$store.state.question.PaperTestHistoryId,
        },
      })
    },
    async submitResult (IsFinished: boolean, noDoneSubmit = true) {
      const data = this.getSubmitData()
      const LastQuestionId = this.getLastQuestion()
      if (!noDoneSubmit && data.realDoneNumber === 0) {
        return Promise.resolve()
      }
      const { CourseId, PaperId, Type, ClassId } = this.$route.query
      return this.$axios({
        url: 'ytjysite/TPaper/SubmitPaper',
        data: {
          PaperId,
          Items: data.answers,
          LastQuestionId,
          PaperTestHistoryId: this.$store.state.question.PaperTestHistoryId,
        },
      }).then(res => {
        return res
      })
    },
    getSubmitData (): SubmitData {
      const answers: SubmitAnswer[] = []
      // const rightQuestionId: number[] = []
      let realDoneNumber = 0
      let doneNumber = 0
      let unDoneNumber = 0
      let rightNumber = 0
      let score = 0
      let totalScore = 0
      for (let question of this.question.QuestionList) {
        if (question.QuestionId === '') {
          continue
        }
        // 类型  1.单选，2.多选，3.判断，4.填空 5配伍 6案例分析选择题 7综合分析选择题 8主观题 9综合分析简答题
        const isSubjectiv = question.QuestionTypeId === 8 || question.QuestionTypeId === 9 // 主观题或综合分析简答题
        // const isFillBlank = question.QuestionTypeId === 8 // 填空题
        if (question.IsRight !== 0 || isSubjectiv) {
          doneNumber++
          if (isSubjectiv) {
            // score += question.QuestionScore
            rightNumber++
          } else {
            realDoneNumber++
            if (question.IsRight === 1) {
              // score += question.QuestionScore
              rightNumber++
            }
          }
        } else {
          unDoneNumber++
        }
        // 计算总分
        // if (isFillBlank) {
        //   totalScore += question.QuestionScore * question.OptionList.length
        // } else {
        //   totalScore += question.QuestionScore
        // }
        answers.push({
          // IsRight: question.IsRight === 1 || isSubjectiv ? 1 : 0,
          Status: question.IsRight === 1 || isSubjectiv ? 1 : question.IsRight,
          QuestionItemId: question.QuestionItemId,
          InputAnswer: '',
          // OptionStr: question.OptionList.map(o => o.OptionId).join(','),
          QuestionId: question.QuestionId,
          // QuestionTypeId: question.QuestionTypeId,
          // UserAnswer: question.UserAnswer,
          ChoosedIdList: question.UserAnswer,
        })
      }
      sessionStorage.setItem('scoreInfo', JSON.stringify({
        score,
        totalScore,
        rightNumber,
        totalNumber: doneNumber + unDoneNumber,
      }))
      return {
        doneNumber,
        realDoneNumber,
        unDoneNumber,
        score: 100,
        answers,
        rightNumber,
      }
    },
    getLastQuestion () {
      let lastIndex = 0
      for (let i = 0; i < this.question.QuestionList.length; i++) {
        if (this.question.QuestionList[i].IsRight !== 0) {
          lastIndex = i
        }
      }
      if (lastIndex >= 0) {
        return lastIndex >= 0 && this.question.QuestionList[lastIndex].QuestionId
      }
    },
  },
})

export interface MappedState {
  question: () => QuestionInfo
  [k: string]: any
}

interface SubmitData {
  doneNumber: number // 包括主观题
  realDoneNumber: number // 不包括主观题
  unDoneNumber: number
  score: number | string
  answers: SubmitAnswer[]
  // rightQuestionId: number[] // 不包括主观题
  rightNumber: number // 包括主观题
}
interface SubmitAnswer {
  // IsRight: number
  Status: number
  // OptionStr: string
  QuestionId: string
  QuestionItemId: string
  InputAnswer: string
  // QuestionTypeId: number
  // UserAnswer: string[]
  ChoosedIdList: string[]
}

import Vue from 'vue'

export default Vue.extend({
  data () {
    return {
      classId: this.$route.query.classId || 0,
    }
  },
  methods: {
    // 生成订单
    createOrder () {
      if (this.$store.state.userInfo.UserId === '-1') {
        sessionStorage.setItem('loginFromPage', JSON.stringify({
          from: this.$route.name,
          query: this.$route.query,
        }))
        this.$router.push({ name: 'login' })
        return
      }
      this.$axios({
        url: 'shopping/buyGoodsForWeb',
        data: {
          GoodsInfo: this.classId,
          OrangeKey: localStorage.getItem('orangeKey') || '',
          IsMstation: true,
        },
      }).then(res => {
        this.navPay(res.OrderNumber, res.OrderId)
      })
    },
    // 已经生成订单的直接传参
    navPay (orderNumber: string, orderId: string = '') {
      this.$router.push({
        name: 'pay',
        query: {
          orderId,
          orderNumber,
        },
      })
    },
  },
})

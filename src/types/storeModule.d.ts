interface RootState {
  ip: string
  userInfo: UserInfoState
  instInfo: InstInfo
  question: QuestionInfo
  category: CategoryInfo
}

interface UserInfoState {
  Age?: number
  CardNo?: string
  HeadImgFileName?: string
  PersonId: string
  Phone: string
  PicUrl?: string
  RealName: string
  Sex?: number
  Signature?: string
  UserId: string
  UserName?: string
  Token: '-1',
  HeadPhoto: '',
  Position: '',
}

interface CategoryInfo {
  CategoryId: string
  CategoryName: string
  isAll: boolean
  isXueLi: boolean
  isSpecial: boolean
  IndustryInfo: object
  selectIndex: number
  isAdultEducationInst: boolean
}

interface QuestionInfo {
  QuestionList: Question[]
  PaperName: string
  LastQuestionId: string
  QuestionTotalCount: number
  RightCount: number
  PaperTestHistoryId: string
}
interface Question {
  RightCountForFillBlank: number
  UserEnglishAnswer: any[]
  OptionList: any[]
  PointList: any[]
  QuestionId: string
  QuestionContent: string
  QuestionScore: number
  UserAnswer: any[]
  RightAnswer: string
  IsRight: number
  QuestionSourceType: 3
  IsCollection: false
  UserAllCount: number
  UserErrorCount: number
  AllCount: number
  ErrorCount: number
  RightRate: number
  AllStatistics: string
  QuestionAnalysis: string
  Difficulty: number
  IsSameQuestion: false
  MainQuestionContent: string
  QuestionTypeId: number
  QuestionTypeName: string
  QuestionTypeDesc: string
  Sequence: number
  QuestionItemId: string
}

interface InstInfo {
  ContactPhone?: string
  FooterName?: string
  FooterUrl?: string
  InstitutionAddress?: string
  Mark?: number
  OfficialList?: any[]
  ProtocolName?: string
  QrcodeUrl?: string
  SchoolLogo?: string
  SeoDescribe?: string
  SeoKeyWords?: string
  TrainingInstitutionId: number | string
  TrainingName?: string
  TryQuestionCount: number
  VideoWatermarking: string
  IsScholarInst: boolean
}

declare module 'vue-awesome-swiper';
declare module 'hammerjs';
declare module 'alloyfinger';
declare module 'nativeshare';
declare module 'qrious'
declare module 'tcplayer'
declare module 'dom-to-image-more'
declare module 'vue-cropper'
declare module 'vue-calendar-component'
declare module 'pell'

declare const TcPlayer: any

interface Window {
  questionIndex?: number
  submitedPaperId: string
  webkit: any
  WebViewJavascriptBridge: any
  Vue: any
  Hls: any
  VueDPlayer: any
  browser: any // QQ浏览器
  visitPageCount: number
  isPageLoading: boolean
  // 提供给app使用
  submitTestResult: any
}

interface Document {
  attachEvent?: any
}

interface APPMethods {
  getUserData: any
  goBack: any
  goShare: any
}

declare const AMap: any

interface ShareInfo {
  /**
   * 成绩
   */
  grade: number
  /**
   * 答对题数量
   */
  count: number
  shareUrl: string
  /**
   * 历年真题和模拟试题传递true
   */
  mockOrReal: boolean
}

declare const WeixinJSBridge: {
  invoke(methodName: string, options: any, cb: (res: any) => any): void
}

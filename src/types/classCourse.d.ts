interface VCourseItem {
  AccountStatus: number
  ChapterList: VChapterList[]
  CourseFinishTime: string
  CourseHour: number
  CourseId: number
  CourseImg: string
  CourseName: string
  CourseStartTime: string
  IsAuditions: boolean
  LearnedTime: number
  School: string
  SectionIdStudying: string
  UserLearnedProgress: string
  UserLearnedTime: string
}
interface VChapterList {
  ChapterId: number
  ChapterName: string
  SectionList: VSectionItem[]
}

interface VSectionItem {
  HavedLearnTime: string
  IsAuditions: true
  IsLiveAudition: false
  IsLiving: false
  IsNew: false
  LiveEndTime: any
  LiveStartTime: string
  SectionId: number
  SectionName: string
  Status: number
  TotalTime: string
  Type: number
  VideoId: string
  // 自加
  courseId: number
  chapterId: number
  courseIndex: number
  index: number // 可播放索引
  CourseImg: string
  accountStatus: number // 账号权限
}

import { DirectiveOptions } from 'vue'
import AlloyFinger from 'alloyfinger'

let index = 0
const AFArr = {} as any
const vTouch: DirectiveOptions = {
  bind (el, binding) {
    let af
    if (!el.dataset.af) {
      af = new AlloyFinger(el, binding.value)
      const afId = `af${index++}`
      el.dataset.af = afId
      AFArr[afId] = af
    }
    // for (let eventName in binding.value) {
    //   af.on(eventName.toLowerCase(), binding.value[eventName])
    // }
  },
  unbind (el, binding) {
    const afId = el.dataset.af
    if (!afId) {
      return
    }
    const af = AFArr[afId]
    if (!af) {
      return
    }
    console.log('unbind', afId)
    af.destroy()
    delete AFArr[afId]
    el.dataset.af = ''
  },
}

export default vTouch

// mutations
export const SET_USER_INFO = 'SET_USER_INFO'
export const USER_LOGOUT = 'USER_LOGOUT'
export const SET_BUY_CLASS_STATUS = 'SET_BUY_CLASS_STATUS'

export const SET_INST_INFO = 'SET_INST_INFO'

export const SET_IP = 'SET_IP'

export const SET_QUESTIONS = 'SET_QUESTIONS'
export const SET_QUESTION = 'SET_QUESTION'
export const DEL_QUESTION = 'DEL_QUESTION'
export const CLEAR_QUESTION = 'CLEAR_QUESTION'

export const SET_CLASS_LIST = 'SET_CLASS_LIST'
export const SET_EXAM_FILTER = 'SET_EXAM_FILTER'
export const CLEAR_EXAM = 'CLEAR_EXAM'

export const SET_GOBACKNAME = 'SET_GOBACKNAME'

export const SET_PAYINFO = 'SET_PAYINFO'

export const SET_GLOBAL_DIALOG = 'SET_GLOBAL_DIALOG'
// actions
export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'
export const REGISTER = 'REGISTER'
export const ADD_DELETE_CART = 'ADD_DELETE_CART'

export const FETCH_CLASS_LIST = 'FETCH_CLASS_LIST'
export const FETCH_EXAM_FILTER = 'FETCH_EXAM_FILTER'

// export const SET_CATEGORY = 'SET_CATEGORY'

import Vue from 'vue'
import Vuex from 'vuex'
import { SET_IP, SET_GOBACKNAME, SET_GLOBAL_DIALOG } from './mutations'
import userInfo from './modules/userInfo'
import instInfo from './modules/instInfo'
import question from './modules/question'
// import category from './modules/category'
import payInfo from './modules/payInfo'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    ip: '127.0.0.2',
    goBackName: '',
    showGlobalDialog: '',
  },
  mutations: {
    [SET_IP] (state, ip) {
      state.ip = ip
    },
    [SET_GOBACKNAME] (state, name) {
      state.goBackName = name
    },
    [SET_GLOBAL_DIALOG] (state, dialogName) {
      state.showGlobalDialog = dialogName
    },
  },
  modules: {
    userInfo,
    instInfo,
    question,
    payInfo,
    // category,
  },
})

import { Module } from 'vuex'
import { SET_PAYINFO } from '../mutations'

const payInfo: Module<PayInfoState, any> = {
  state: {
    ClassName: '',
    ClassId: 0,
    ClassImg: '',
    ClassPeople: 0,
    ClassPrice: '',
    ClassTime: '',
    FitPeople: '',
  },
  mutations: {
    [SET_PAYINFO] (state, payload) {
      Object.assign(state, payload)
      localStorage.setItem('payInfo', JSON.stringify(payload))
    },
  },
}

export interface PayInfoState {
  ClassName: string
  ClassImg: string
  ClassPrice: string
  ClassPeople: number
  FitPeople: string
  ClassId: number
  ClassTime: string
}

export default payInfo

import { Module } from 'vuex'
import { SET_INST_INFO } from '../mutations'
import { parseQuery } from 'lib/utils'

const instInfo: Module<InstInfo, any> = {
  state: {
    TrainingInstitutionId: parseQuery(location.search).instId || 0,
    TryQuestionCount: 1,
    VideoWatermarking: '',
    IsScholarInst: false,
  },
  mutations: {
    [SET_INST_INFO] (state, payload) {
      const IsScholarInst = payload.TrainingInstitutionId === 1 ||
          payload.TrainingInstitutionId === 154 ||
          payload.TrainingInstitutionId === 173 ||
          payload.TrainingInstitutionId === 157 ||
          payload.TrainingInstitutionId === 261 ||
          payload.TrainingInstitutionId === 167 ||
          payload.TrainingInstitutionId === 252 ||
          payload.TrainingInstitutionId === 275
      Object.assign(state, { ...payload, IsScholarInst })
    },
  },
}

export default instInfo

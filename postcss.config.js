
module.exports = ({ file }) => {
  let remUnit = 75
  if (file && file.dirname && file.dirname.indexOf('vant') > -1) {
    remUnit = 37.5
  }
  return {
    plugins: {
      autoprefixer: {},
      'cssnano': {},
      'postcss-pxtorem': {
        rootValue: remUnit,
        propList: ['*'],
        minPixelValue: 2,
        // 该项仅在使用 Circle 组件时需要
        // 原因参见 https://github.com/youzan/vant/issues/1948
        selectorBlackList: ['van-circle__layer'],
      },
    },
  }
}

module.exports = {
  root: true,
  env: {
    node: true,
  },
  'extends': [
    'plugin:vue/recommended',
    '@vue/standard',
    '@vue/typescript',
  ],
  rules: {
    'no-console': ['error', { allow: ['log'] }],
    'no-debugger': 2,
    'comma-dangle': [
      'error',
      'always-multiline',
    ],
    'max-len': [2, {
      'code': 100,
      'ignoreTrailingComments': true,
      'ignoreTemplateLiterals': true,
      'ignoreUrls': true,
      'comments': 150,
    }],
    'object-shorthand': ['error', 'properties'],
    'no-var': 2,
    'no-extra-semi': 2,
    'vue/eqeqeq': 2,
    'vue/key-spacing': 2,
    // "vue/keyword-spacing": 2,
    'vue/block-spacing': 2,
  },
  parserOptions: {
    parser: 'typescript-eslint-parser',
  },
}

const path = require('path')
const TerserPlugin = require('terser-webpack-plugin')

module.exports = {
  lintOnSave: process.env.NODE_ENV !== 'production' ? true : 'error',
  productionSourceMap: process.env.sm === '1',
  crossorigin: 'anonymous',
  // publicPath: process.env.NODE_ENV === 'production' ? 'https://devappexam.huikao8.cn' : '/',

  // transpileDependencies: ['hammerjs'],
  configureWebpack: config => {
    config.resolve.alias = {
      ...config.resolve.alias,
      'scss': path.resolve(__dirname, 'src/scss'),
      'assets': path.resolve(__dirname, 'src/assets'),
      'components': path.resolve(__dirname, 'src/components'),
      'lib': path.resolve(__dirname, 'src/lib'),
      jquery: path.join(__dirname, 'node_modules', 'jquery', 'dist', 'jquery'),
    }
    if (process.env.VUE_APP_FOR_TEST !== '1') {
      config.optimization = {
        minimizer: [new TerserPlugin({ terserOptions: { compress: { drop_console: true } } })],
      }
    }
  },
  chainWebpack: config => {
    config.module.rule('eslint').use('eslint-loader').tap(option => {
      option.emitWarning = true
      option.failOnError = true
      option.failOnWarning = true
      return option
    })
    // config.plugin('define').tap(definitions => {
    //   definitions[0] = Object.assign(definitions[0], {
    //     $: 'jquery',
    //     jquery: 'jquery',
    //     'window.jQuery': 'jquery',
    //     jQuery: 'jquery',
    //   })
    //   return definitions
    // })
  },
  devServer: {
    disableHostCheck: true,
    overlay: {
      warnings: true,
      errors: true,
    },
    proxy: {
      '/api': {
        target: 'http://81.70.41.226:8003/',
        ws: true,
        changeOrigin: true,
      },
    },
  },

  css: {
    loaderOptions: {
      sass: {
        data: '@import "@/scss/_variables.scss";',
      },
      less: {
        modifyVars: require('./src/vant/theme'),
      },
    },
  },
}
